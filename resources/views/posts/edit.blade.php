@extends('layouts.app')

@section('content')

    <h3 class="font-semibold mb-2 text-xl leading-tight sm:leading-normal">編修</h3>

    {!! Form::open(['action' => ['PostsController@update', $post->id], 'id' => 'form', 'method' => 'POST', 'enctype' =>
    'multipart/form-data']) !!}
    <div class="form-group">
        {{ Form::label('publish', '公開') }}
        {{ Form::checkbox('publish', 'publish', $post->publish) }}
    </div>
    <div class="form-group">
        {{ Form::text('title', $post->title, ['class' => 'form-control', 'placeholder' => 'title']) }}
    </div>
    <div class="form-group" style="display: none;">
        {{ Form::label('body', 'Body') }}
        {{ Form::textarea('body', $post->body, [
    'id' => 'body',
    'class' => 'form-control',
]) }}
    </div>
    <div class="form-group">
        <div class="editorjs-bg">
            <div id="editorjs"></div>
        </div>
    </div>
    <div class="form-group">
        {{ Form::label('type', 'Type') }}
        {{ Form::text('type', $post->type, ['class' => 'form-control', 'placeholder' => 'seperate by space']) }}
    </div>

    <div class="form-group">
        {{ Form::label('cover_image', 'Cover Image') }}
        {{ Form::file('cover_image') }}

        @if ($post->cover_image != '')


            <div class="bg-white overflow-hidden border-b-4 border-blue-500 w-1/3">
                <img src="{{ asset('storage/posts_images') . '/' . $post->cover_image }}" />

            </div>
        @endif
    </div>

    <div class="form-group">
        {{ Form::label('attach', '附檔') }}
        {{ Form::file('attach') }}

        @if ($post->attach != '')

            <div class="bg-white overflow-hidden border-b-4 border-blue-500 w-1/3">
                <a href="{{ asset('storage/attach') . '/' . $post->attach }}" download="download">下載</a>

            </div>

        @endif
    </div>

    <div class="form-group">
        {{ Form::label('hashtags', 'Hashtags') }}
        {{ Form::text('hashtags', $post->hashtags, ['class' => 'form-control', 'placeholder' => 'seperate by space']) }}
    </div>



    {{ Form::hidden('_method', 'PUT') }}
    {{ Form::hidden('_postid', $post->id, ['id' => 'postid']) }}
    {{ Form::submit('存檔', ['class' => 'btn btn-primary']) }}
    <a href="/posts/{{ $post->id }}" class="btn btn-primary">預覽頁</a>
    <a href="/manager" class="btn btn-primary">管理頁</a>
    {!! Form::close() !!}


@endsection
@section('style')
    <style>
        .editorjs-bg {
            padding: 20px;
            background-color: rgb(235 235 235 / 49%);
            border-radius: 20px;
            border: 1px solid #ccc;
        }

        #editorjs {
            background: #fff;
        }

    </style>
@endsection
@section('js')

    <script src="{{ asset('js/view/posts/edit.js') }}"></script>

@endsection
