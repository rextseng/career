<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Sitemap;

class SitemapController extends Controller
{
    public function __construct()
    {
     
    }   
    public function index()
    {
        $posts = Post::where('publish',true)->orderBy('updated_at','desc')->get();

      return response()->view('sitemap.index', [
          'posts' => $posts
      ])->header('Content-Type', 'text/xml');
    }
}