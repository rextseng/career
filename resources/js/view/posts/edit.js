import EditorJS from '@editorjs/editorjs';
import Header from '@editorjs/header';
import List from '@editorjs/list';
import Paragraph from '@editorjs/paragraph'
import Quote from '@editorjs/quote';
import Delimiter from '@editorjs/delimiter'
import Checklist from '@editorjs/checklist';
import LinkTool from '@editorjs/link';
import ImageTool from '@editorjs/image';
import Table from '@editorjs/table';
import Code from '@editorjs/code';
import Raw from '@editorjs/raw';
import InlineCode from '@editorjs/inline-code';
import Marker from '@editorjs/marker';


const axios = require('axios');
var $ = window.jQuery = require('jquery');

class SimpleImage {
    static get toolbox() {
        return {
            title: 'Image',
            icon: '<svg width="17" height="15" viewBox="0 0 336 276" xmlns="http://www.w3.org/2000/svg"><path d="M291 150V79c0-19-15-34-34-34H79c-19 0-34 15-34 34v42l67-44 81 72 56-29 42 30zm0 52l-43-30-56 30-81-67-66 39v23c0 19 15 34 34 34h178c17 0 31-13 34-29zM79 0h178c44 0 79 35 79 79v118c0 44-35 79-79 79H79c-44 0-79-35-79-79V79C0 35 35 0 79 0z"/></svg>'
        };
    }

    render() {
        return document.createElement('input');
    }

    save(blockContent) {
        return {
            url: blockContent.value
        }
    }
}

const editor = new EditorJS({
    /** 
     * Id of Element that should contain the Editor 
     */
    holder: 'editorjs',

    /** 
     * Available Tools list. 
     * Pass Tool's class or Settings object for each Tool you want to use 
     */
    tools: {
        checklist: {
            class: Checklist,
            inlineToolbar: true,
        },
        header: {
            class: Header,
            inlineToolbar: true
        },
        paragraph: {
            class: Paragraph,
            inlineToolbar: true,
        },
        quote: {
            class: Quote,
            inlineToolbar: true,
            shortcut: 'CMD+SHIFT+O',
            config: {
                quotePlaceholder: 'Enter a quote',
                captionPlaceholder: 'Quote\'s author',
            },
        },
        delimiter: Delimiter,
        inlineCode: {
            class: InlineCode,
            shortcut: 'CMD+SHIFT+M',
        },
        linkTool: {
            class: LinkTool,
            config: {
                endpoint: '/fetchUrl', // Your backend endpoint for url data fetching
            }
        },
        list: List,
        Marker: {
            class: Marker,
            shortcut: 'CMD+SHIFT+M',
        },
        table: {
            class: Table,
            inlineToolbar: true,
            config: {
                rows: 2,
                cols: 3,
            },
        },
        image: {
            class: ImageTool,
            config: {
                endpoints: {
                    byFile: '/upload', // Your backend file uploader endpoint
                    byUrl: '/upload', // Your endpoint that provides uploading by Url
                },
                additionalRequestHeaders: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                additionalRequestData: {
                    postid: document.getElementById('postid').value
                }
            }
        },
        code: Code,
        raw: Raw
    },
    data: {},//JSON.parse(response.data.body)
    placeholder: 'Let`s write an awesome story!',
    autofocus: true
});

editor.isReady
    .then(() => {

        let postid = document.getElementById('postid').value;
        axios({
            method: 'get',
            url: '/data/posts/' + postid
        }).then(function (response) {
            console.log(response);
            try {
                editor.render(JSON.parse(response.data.body));
            } catch (err) {
                editor.render({
                    version: '',
                    time: '',
                    blocks: [
                        {
                            type: 'paragraph',
                            data: {
                                text: response.data.body
                            }
                        }
                    ]
                });
            }

        });;
    })
    .catch((reason) => {
    });

/// 儲存
document.getElementById('form').onsubmit = function () {
    editor.save().then((outputData) => {

        let result = JSON.stringify(outputData);
        document.getElementById('body').innerText = result;
    }).catch((error) => {

    });
}

