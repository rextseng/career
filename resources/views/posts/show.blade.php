@extends("layouts.blog")

@section('head')
    <meta name="description" content="{{ $post->title }}">
    <meta name="keywords" content="{{ str_replace(';', ',', $post->hashtags) }}">
@endsection
@section('content')

    @php
    $hashtags = explode(';', $post->hashtags);
    @endphp

    <!--========== header =============-->
    <h1 class="text-5xl text-gray-900 mb-3 rexcode-title">{{ $post->title }}</h1>

    <!--========== information ========-->
    <div class="container post-margin-bottom">
        <div class="row">
            <div class="col-sm text-gray-500">
                {{ $post->created_at }}
            </div>
            <div class="col-sm">
                <div class="text-base md:text-sm text-gray-500 px-4 py-6">
                    Tags:
                    @if (is_null($post->hashtags) == false)

                        @for ($i = 0; $i < count($hashtags); $i++)
                            <a href="/post?tag={{ $hashtags[$i] }}"
                                class="text-base md:text-sm text-green-500 no-underline hover:underline">{{ $hashtags[$i] }}</a>
                            .
                        @endfor
                    @endif

                </div>

            </div>
        </div>
    </div>

    <!--=========== body ==========-->
    <div id="app">

        <p v-for="block in blocks" class="text-gray-900 text-base">

            <paragraph-component v-if="block.type === 'paragraph'" :text="block.data.text"></paragraph-component>

            <image-component v-if="block.type === 'image'" :imagesrc="block.data.file.url" :caption="block.data.caption">
            </image-component>

            <code-component v-if="block.type === 'code'" :text="block.data.code"></code-component>

            <quote-component v-if="block.type === 'quote'" :text="block.data.text" :caption="block.data.caption">
            </quote-component>

            <delimiter-component v-if="block.type === 'delimiter'"></delimiter-component>

            <raw-component v-if="block.type === 'raw'" :html="block.data.html"></raw-component>

            <header-component v-if="block.type === 'header'" :text="block.data.text" :level="block.data.level">
            </header-component>

            <list-component v-if="block.type === 'list'" :items="block.data.items"></list-component>
        </p>
    </div>

    @if ($post->attach != '')
        <div class="m-2">
            <a href="{{ asset('storage/attach') . '/' . $post->attach }}" download="{{ $post->name }}">
                <button
                    class="py-2 px-4 bg-green-500 text-white font-semibold rounded-lg shadow-md hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-green-400 focus:ring-opacity-75">
                    範例下載
                </button>
            </a>
        </div>
    @endif
    <a href="/"
        class="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center">列表</a>

    <!--======== Manager =========-->
    @if (!auth()->guest())
        <a href="/posts/{{ $post->id }}/edit" class="btn btn-primary">編輯</a>

        {!! Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'POST', 'class' => 'pull-right', 'style' => 'display:inline-block']) !!}

        {{ Form::hidden('_method', 'DELETE') }}
        {{ Form::submit('刪除', ['class' => 'btn btn-primary']) }}
        {!! Form::close() !!}
    @endif

    {{ Form::hidden('_postid', $post->id, ['id' => 'postid']) }}


    <div class="hidden">
        {{ $post->body }}
    </div>
    <br /><br />
@endsection

@section('js')
    <script src="{{ asset('js/view/posts/show.js') }}"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.js'></script>
    <script src="https://cdn.jsdelivr.net/gh/google/code-prettify@master/loader/run_prettify.js?autorun=true" defer>
    </script>
    <style>
        pre.prettyprint {
            padding: 2px;
            border: 0px solid rgb(187, 187, 187);
        }

        .rexcode-paragraph a {
            color: rgb(44, 129, 185);
        }

    </style>
@endsection
