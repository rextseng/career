@extends('layouts.app')

@section('content')

    @if (count($posts) > 0)

        <table class="table-auto">
            <thead>
                <tr>
                    <th class="px-4 py-2">標題</th>
                    <th class="px-4 py-2">修改</th>
                    <th class="px-4 py-2">公開</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($posts as $post)
                    <tr>
                        <td class="border px-4 py-2">
                            <a href="/posts/{{ $post->id }}">{{ $post->title }}</a></td>
                        <td class="border px-4 py-2">
                            <a href="/posts/{{ $post->id }}/edit">
                                <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-0 px-2 rounded-full">
                                    edit
                                </button>
                            </a>

                        </td>
                        <td class="border px-4 py-2">
                            @if ($post->publish == true)
                                <span class="badge badge-success">online</span>
                            @else
                                <span class="badge badge-secondary">off</span>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif
@endsection
