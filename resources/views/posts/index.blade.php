@extends('layouts.app')

@section('content')

    @if(count($posts) > 0)

        @foreach($posts as $post)
            <div class="well mb-4">
                <div><a href="/posts/{{$post->id}}" class="text-3xl text-blue-700 rexcode-title">{{$post->title}}</a></div>
                <div>
                    {!! $post->content !!}
                </div>
                <small>{{$post->created_at}}</small>
            </div>

        @endforeach
        
    @else
        <p>no posts</p>
    @endif
@endsection