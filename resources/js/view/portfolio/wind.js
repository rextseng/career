import 'ol/ol.css';
import { Map, View } from 'ol';
import TileLayer from 'ol/layer/Tile';
import OSM from 'ol/source/OSM';
window.Vue = require('vue');

import Wind from './windtool';

const app = new Vue({
    el: 'pageapp',
    beforeMount() {
        window.map = new Map({
            target: 'map',
            layers: [
                new TileLayer({
                    source: new OSM()
                })
            ],
            view: new View({
                center: [13446557.328433467, 2729065.9426927143],
                zoom: 6
            })
        });
        window.map.on('movestart', function () {
            Wind.redraw();

        });
        setTimeout(function () {

            Wind.add_wind();
        }, 200);

    }
});



