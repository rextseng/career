<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });
Route::get('/', 'IndexController@index');
Route::get('/proxy', 'IndexController@proxy');
Route::get('/fetchUrl', 'IndexController@fetchUrl');
Auth::routes();

Route::resource('posts', 'PostsController');

Route::resource('map', 'MapController');
// manager
Route::resource('manager', 'ManagerController');

// get post draw data
Route::get('/data/posts/{id}', 'PostsController@get_posts_body');
Route::post('/upload', 'PostsController@upload');

Route::get('/test', function () {
    return File::get(public_path() . '/test.html');
});

//# sitemap
Route::get('/sitemap.xml', 'SitemapController@Index');

//# Protfolio
Route::get('/portfolio', 'ProtfolioController@Index');
Route::get('/portfolio/wind', 'ProtfolioController@Wind');
Route::get('/portfolio/roadmap', 'ProtfolioController@Roadmap');
Route::get('/portfolio/jargon', 'ProtfolioController@jargon');