const mix = require('laravel-mix');

const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');
const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin')


var webpackConfig = {
    plugins: [
        require('tailwindcss'),
        require('autoprefixer'),
        new VuetifyLoaderPlugin(),
        new CaseSensitivePathsPlugin()
        // other plugins ...
    ]
    // other webpack config ...
}
mix.webpackConfig(webpackConfig);
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    //posts
    .js('resources/js/view/posts/create.js', 'public/js/view/posts/')
    .js('resources/js/view/posts/edit.js', 'public/js/view/posts/')
    .js('resources/js/view/posts/show.js', 'public/js/view/posts/')
    //wind
    .js('resources/js/view/portfolio/wind.js', 'public/js/view/portfolio/')
    //map
    .js('resources/js/view/map/index.js', 'public/js/view/map/')
    .sass('resources/sass/app.scss', 'public/css');

const tailwindcss = require('tailwindcss')

mix.sass('resources/sass/tailand.scss', 'public/css')
    .options({
        processCssUrls: false,
        postCss: [tailwindcss('tailwind.config.js')],
    })