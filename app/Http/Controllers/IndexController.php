<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class IndexController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $posts = Post::where('publish', true)->orderBy('updated_at', 'desc')->get();

        for ($i = 0; $i < count($posts); $i++) {
            $content = "";
            $bodys = json_decode($posts[$i]["body"]);
            //return $posts[$i]["body"];
            foreach ($bodys->blocks as $body) {

                if ($body->type == 'paragraph') {
                    $content .= $body->data->text;
                    break;
                }
            }
            $posts[$i]["content"] = $content;
        }

        return View('posts.index')->with('posts', $posts);
    }

    public function proxy(Request $request)
    {

        $url = $request->input('url');

        return file_get_contents($url);
    }

    public function fetchUrl(Request $request)
    {

        $url = $request->input('url');

        return file_get_contents($url);
    }
}