<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Logs extends Model
{
    //
    protected $table = 'logs';

    // Primary Key
    public $primaryKey = 'id';

    public $timestamps = true;
}
