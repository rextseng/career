
var moment = require('moment'); // require
var axios = require('axios');
import { toLonLat } from 'ol/proj';

//風力
var Wind = {
    windy: null,
    windlayer: null,
    windcanvas: null,
    add_wind: function () {

        this.windlayer = document.getElementsByClassName('ol-layer');

        this.windcanvas = document.createElement('canvas');
        this.windcanvas.id = "CursorLayer";
        this.windcanvas.width = 1224;
        this.windcanvas.height = 768;
        this.windcanvas.style.zIndex = 99999999999999;
        this.windcanvas.style.position = "absolute";
        this.windcanvas.style.border = "0px solid";
        this.windcanvas.style.left = "0px";
        this.windcanvas.style.top = "0px";

        this.windlayer[0].appendChild(this.windcanvas);

        //https://aq.epb.taichung.gov.tw/tccgAIR/GFS/gfs.json
        //https://winfo.tycg.gov.tw/TYSAFEP/json/proxy.ashx?Name=wind&start=2020-10-22%2008:00&end=2020-10-22%2023:00
        var start = moment().subtract(2, 'hours').format('YYYY-MM-DD HH:mm:ss');
        var end = moment().subtract(0, 'hours').format('YYYY-MM-DD HH:mm:ss');


        axios({
            method: 'get',
            url: "/proxy?url=https://winfo.tycg.gov.tw/TYSAFEP/json/proxy.ashx?Name=wind&start=" + start + "&end=" + end
        }).then(function (response) {
            console.log(response);
            let data = response.data.data[0].grib_json;
            Wind.windy = new Windy({ canvas: Wind.windcanvas, data: data });
            Wind.redraw();
        });;

    },
    remove_wind: function () {
        if (this.windlayer != null) {

            //map.mm.map.removeLayer(this.windlayer);
            this.windlayer = null;
        }
    },
    redraw: function () {

        if (Wind.windcanvas == null) return;

        var w = document.getElementById('map').clientWidth;
        var h = document.getElementById('map').clientHeight;

        Wind.windcanvas.width = w;
        Wind.windcanvas.height = h;

        Wind.windy.stop();

        map.getView().calculateExtent(map.getSize())
        // Nowadays, map.getSize() is optional most of the time (except if you don't share view between different maps), so above is equivalent to the following
        var extent = map.getView().calculateExtent()
        var lt = toLonLat([extent[0], extent[3]]);
        var rb = toLonLat([extent[2], extent[1]]);

        //return [lt.lon,
        //      lt.lat,
        //      rb.lon,
        //      rb.lat];

        setTimeout(function () {
            Wind.windy.start(
                [[0, 0], [w, h]],
                w,
                h,
                [[lt[0], rb[1]], [rb[0], lt[1]]]
            );
        }, 500);
    }
}

export default Wind;