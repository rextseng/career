window.Vue = require('vue');
//import Vuetify from "../../plugins/vuetify"
const axios = require('axios');
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

//Vue.component('example-component', require('../components/ExampleComponent.vue').default);
Vue.component('paragraph-component', require('../../components/posts/ParagraphComponent.vue').default);
Vue.component('image-component', require('../../components/posts/ImageComponent.vue').default);
Vue.component('code-component', require('../../components/posts/CodeComponent.vue').default);
Vue.component('quote-component', require('../../components/posts/QuoteComponent.vue').default);
Vue.component('delimiter-component', require('../../components/posts/DelimiterComponent.vue').default);
Vue.component('raw-component', require('../../components/posts/RawComponent.vue').default);
Vue.component('header-component', require('../../components/posts/HeaderComponent.vue').default);
Vue.component('list-component', require('../../components/posts/ListComponent.vue').default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    //vuetify: Vuetify,
    el: '#app',
    data: {
        post: null,
        blocks: null
    },
    beforeMount() {

        let self = this;
        let postid = document.getElementById('postid').value;
        axios({
            method: 'get',
            url: '/data/posts/' + postid
        }).then(function (response) {
            response.data.body = JSON.parse(response.data.body);
            self.post = response;
            self.blocks = response.data.body.blocks;
        });;

    }
});
window.vueapp = app;