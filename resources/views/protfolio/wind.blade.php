@extends('layouts.app')

@section('content')

    <div id="pageapp">
        <div id="map"></div>
    </div>

@endsection

@section('style')
    <style>
        #map {
            width: 100%;
            height: 400px;
        }

    </style>
@endsection

@section('js')
    <script src="{{ asset('js/view/portfolio/windy.js') }}"></script>
    <script src="{{ asset('js/view/portfolio/wind.js') }}"></script>

@endsection
