@extends('layouts.app')


@section('content')
    <h3 class="font-semibold mb-2 text-xl leading-tight sm:leading-normal">新增</h3>

    {!! Form::open(['action' => 'PostsController@store', 'id' => 'form', 'method' => 'POST', 'enctype' =>
    'multipart/form-data']) !!}

    <div class="form-group">

        {{ Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'title']) }}
    </div>
    <div class="form-group hidden" style="display: none;">
        {{ Form::label('body', 'Body') }}
        {{ Form::textarea('body', '', [
                    'id' => 'body',
                    'class' => 'form-control',
                ]) }}

    </div>
    <div class="form-group hidden">
        <div class="editorjs-bg">
            <div id="editorjs"></div>
        </div>

    </div>
    <div class="form-group hidden">
        {{ Form::label('cover_image', 'Cover Image') }}
        {{ Form::file('cover_image') }}
    </div>
    <div class="form-group hidden">
        {{ Form::label('publish', 'publish') }}
        {{ Form::checkbox('publish', 'Publish', false) }}
    </div>
    <div class="form-group">
        {{ Form::label('type', '分類') }}
        {{ Form::text('type', 'IT', ['class' => 'form-control', 'placeholder' => 'title']) }}
    </div>
    <div class="form-group hidden">
        {{ Form::label('hashtags', 'Hashtags') }}
        {{ Form::text('hashtags', '', ['class' => 'form-control', 'placeholder' => 'seperate by space']) }}
    </div>
    {{ Form::submit('Submit', ['class' => 'btn btn-primary']) }}
    {!! Form::close() !!}

@endsection

@section('style')
    <style>
        .editorjs-bg {
            padding: 20px;
            background-color: rgb(235 235 235 / 49%);
            border-radius: 20px;
            border: 1px solid #ccc;
        }

        #editorjs {
            background: #fff;
        }

    </style>
@endsection
@section('js')
    <script src="{{ asset('js/view/posts/create.js') }}"></script>
@endsection
