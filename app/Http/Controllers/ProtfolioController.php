<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProtfolioController extends Controller
{
    public function index()
    {
        return View('protfolio.index');
    }

    public function wind()
    {
        return View('protfolio.wind');
    }

    public function roadmap()
    {
        return View('protfolio.roadmap');
    }
    public function jargon()
    {
        return View('protfolio.jargon');
    }
}