<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Logs;
use DB;


class PostsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show', 'get_posts_body']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $f = $request->input('f');
        $tag = $request->input('tag');

        if (is_null($f)) $f = 'article';
        //$posts = Post::all();
        //$posts = Post::orderBy('title','desc')->get();
        //$posts = Post::where('title','Post two')->get();
        //$posts = DB::select('SELECT * FROM posts');
        //$posts = Post::orderBy('title','desc')->take(1)->get();
        //$posts = Post::orderBy('title','desc')->paginate(1);
        if (is_null($tag)) {
            $posts = Post::where('type', $f)->where('publish', true)->orderBy('updated_at', 'desc')->get();
        } else {
            $tag = strtolower($tag);
            $posts = Post::orWhere('hashtags', 'like', '%' . $tag . '%')->where('publish', true)->orderBy('updated_at', 'desc')->get();
        }


        for ($i = 0; $i < count($posts); $i++) {
            $content = "";
            $bodys = json_decode($posts[$i]["body"]);
            //return $posts[$i]["body"];
            foreach ($bodys->blocks as $body) {

                if ($body->type == 'paragraph') {
                    $content .= $body->data->text;
                }
            }
            $posts[$i]["content"] = $content;
        }

        //logs
        $logs = new Logs();
        $logs->ip = '127.0.0.1';
        $logs->url = '/posts';
        $logs->save();

        return View('posts.index')->with('posts', $posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("posts.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            'cover_image' => 'image|nullable|max:1999' //under 2 MB
        ]);

        // handle file upload
        if ($request->hasFile('cover_image')) {

            $cover_image = $request['cover_image'];
            // get file name without extension
            $filename = pathinfo($cover_image->getClientOriginalName(), PATHINFO_FILENAME);
            // get just ext
            $extension = $cover_image->clientExtension();
            // filename to store
            $fileNameToStore = $filename . '-' . time() . '.' . $extension;
            // ppload image
            $path = $cover_image->storeAs('public/posts_images', $fileNameToStore);
        } else {
            $fileNameToStore = '';
        }

        // Create post
        $post = new Post;
        $post->publish = $request->input('publish') ? true : false;
        $post->type = $request->input('type');
        $post->hashtags = $request->input('hashtags');
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->user_id = auth()->user()->id;
        $post->cover_image = $fileNameToStore;
        $post->save();



        return redirect('/posts/' . $post->id . '/edit');
    }

    public function upload(Request $request)
    {

        $image = $request['image'];
        $postid = $request['postid'];


        // get file name without extension
        $filename = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
        // get just ext
        $extension = $image->clientExtension();
        // filename to store
        $fileNameToStore = $filename . '-' . time() . '.' . $extension;
        // ppload image
        $path = $image->storeAs('public/posts_images/' . $postid, $fileNameToStore);

        return response()->json(array(
            'success' => 1,
            'file' => array(
                'url' => '/storage/posts_images/' . $postid . '/' . $fileNameToStore
            )
        ));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        return view('posts.show')->with('post', $post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        return view('posts.edit')->with('post', $post);
    }

    /**
     * get posts body by $id
     * 
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function get_posts_body($id)
    {
        $post = Post::find($id);
        return $post;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            'cover_image' => 'image|nullable|max:1999' //under 2 MB
        ]);

        $post = Post::find($id);
        $post->publish = $request->input('publish') ? true : false;
        $post->type = $request->input('type');
        $post->hashtags = $request->input('hashtags');
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->user_id = auth()->user()->id;

        // handle file upload
        if ($request->hasFile('cover_image')) {

            $cover_image = $request['cover_image'];
            // get file name without extension
            $filename = pathinfo($cover_image->getClientOriginalName(), PATHINFO_FILENAME);
            // get just ext
            $extension = $cover_image->clientExtension();
            // filename to store
            $fileNameToStore = $filename . '-' . time() . '.' . $extension;
            // ppload image
            $path = $cover_image->storeAs('public/posts_images', $fileNameToStore);

            $post->cover_image = $fileNameToStore;
        }

        // handle attach file
        if ($request->hasFile('attach')) {
            $attach = $request['attach'];
            // get file name without extension
            $filename = pathinfo($attach->getClientOriginalName(), PATHINFO_FILENAME);
            // get just ext
            $extension = $attach->clientExtension();
            // filename to store
            $fileNameToStore = $filename . '-' . time() . '.' . $extension;
            // ppload image
            $path = $attach->storeAs('public/attach', $fileNameToStore);

            $post->attach = $fileNameToStore;
        }


        $post->save();

        return redirect('/posts/' . $id . '/edit')->with('success', 'Post updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $post->delete();
        return redirect('/posts')->with('success', 'Post deleted');
    }
}