<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-177207415-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-177207415-1');

    </script>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @yield('head')

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'CAREER') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/tailand.css') }}" rel="stylesheet">

    <!-- Custom Css Every Page -->
    @yield('style')
</head>

<body class="bg-light">

    <div id="app">
        @include('inc.navbar')
        <div class="container">
            <div class="row">
                @include('inc.messages')
            </div>
            <div class="row">

                <!-- Post Content Column -->
                <div class="col-lg-7  p-10">
                    @yield('content')
                </div>

                <!-- Sidebar Widgets Column -->
                <div class="col-md-5">
                    <div class="card my-4" style="display: none">
                        <h5 class="card-header">分類</h5>
                        <div class="card-body">

                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
        </div>
        <footer class="container">
            <p>&copy; Since 2020</p>
        </footer>
    </div>
    @yield('js')
</body>

</html>
